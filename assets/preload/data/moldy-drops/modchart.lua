local bgs = {"sky","peaks","floor","black","antena","cliffN"} --Images variables

function start(song) -- do nothing
	loadCharacter("pico",100,400)
    loadBG()
end

function update(elapsed)
   
end

function beatHit(beat) -- do nothing

end

function stepHit(step) -- do nothing
    if curStep == 16 then --Swaps the rival with Pico and shows the alternative background
		flash(255,0,0,1)
        changeDadCharacter("pico")
		tweenFadeIn(bgs[1],1,0.1)
		tweenFadeIn(bgs[2],1,0.1)
		tweenFadeIn(bgs[3],1,0.1)
		tweenFadeIn(bgs[5],1,0.1)
		tweenFadeIn(bgs[6],1,0.1)
    end
	if curStep == 128 then --puts the default background
		tweenFadeIn(bgs[1],0.001,0.1)
		tweenFadeIn(bgs[2],0.001,0.1)
		tweenFadeIn(bgs[3],0.001,0.1)
		tweenFadeIn(bgs[5],0.001,0.1)
		tweenFadeIn(bgs[6],0.001,0.1)
	end
end

function playerTwoTurn()
end

function playerOneTurn()
end

function playerTwoSing(direccion, pos, tipo, isSustained)
   
end

function playerOneSing(direccion, pos, tipo, isSustained)
   
end

function loadBG() --This function adds plains images on the background
	makeSprite("outerspace",bgs[1],true)
	setActorScale(2,bgs[1])
	setActorX(0,bgs[1])
	setActorY(130,bgs[1])
	setScrollFactor(0,0,bgs[1])
	setAntialiasing(true,bgs[1])
	tweenFadeOut(bgs[1],0.001,0.1)
	makeSprite("SpikeNight2",bgs[2],true)
	setActorScale(1.8,bgs[2])
	setActorX(120,bgs[2])
	setActorY(275,bgs[2])
	setScrollFactor(0.15,0.5,bgs[2])
	setAntialiasing(true,bgs[2])
	tweenFadeOut(bgs[2],0.001,0.1)
	makeSprite("SpikeNight",bgs[3],true)
	setActorScale(1.8,bgs[3])
	setActorX(0,bgs[3])
	setActorY(320,bgs[3])
	setScrollFactor(0.25,0.7,bgs[3])
	setAntialiasing(true,bgs[3])
	tweenFadeOut(bgs[3],0.001,0.1)
	makeSprite("black",bgs[4],true)
	setActorScale(2,bgs[4])
	setActorX(-270,bgs[4])
	setActorY(130,bgs[4])
	setScrollFactor(0,0,bgs[4])
	setActorAlpha(0,bgs[4])
	makeSprite("antenaN",bgs[5],true)
	setActorScale(0.9,bgs[5])
	setActorX(-210,bgs[5])
	setActorY(360,bgs[5])
	setScrollFactor(0.35,0.8,bgs[5])
	setAntialiasing(true,bgs[5])
	tweenFadeOut(bgs[5],0.001,0.1)
	makeSprite("cliffN",bgs[6],true)
	setActorScale(1.6,bgs[6])
	setActorX(170,bgs[6])
	setActorY(990,bgs[6])
	setAntialiasing(true,bgs[1])
	tweenFadeOut(bgs[6],0.001,0.1)
end