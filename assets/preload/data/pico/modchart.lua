local bob = "ojo"

function start(song)
	loadCharacter("bf-pixel", getActorX("boyfriend")+270, getActorY("boyfriend")+170, true)
	changeBoyfriendCharacter("bf-pixel")
	loadCharacter("tankman", -50, 50)
	loadCharacter("pico", getActorX("dad"), getActorY("dad"))
	changeDadCharacter("pico")
	visibleChar("tankman", true, false)

	loadGirlfriend("gf-pixel", 630, 430)
	

	makeSprite("bobscreen",bob,false)
	setActorX(20, bob)
	setActorY(50, bob)
	setActorScale(1.15, bob)
	setScrollFactor(0,0, bob)
	setActorAlpha(0, bob)
end

function update(elapsed)
	
end

function stepHit(step)
	cambio()
	if step == 4 then
		--flash(234, 169, 41, 1.5)
		setActorAlpha(1, bob)
		changeGirlfriendCharacter("gf-pixel")
	end
	if step == 6 then
		tweenFadeOut(bob, 0, 1.5)
	end
	if step >= 38 and step <= 95 then
		shakeCam(0.01, 0.1)
		if getHealth() >= 0.5 then
			heal(-0.01)
		end
	end
	if step >= 162 and step <= 220 then
		shakeCam(0.01, 0.1)
		if getHealth() >= 0.5 then
			heal(-0.02)
		end
	end
end

function beatHit(beat)

end

function cambio()
	if curStep == 162 then
		--flash(0,0,0,1.5)
		setActorAlpha(1, bob)
		changeDadCharacter("tankman")
		visibleChar("pico",true,false)
	end
	if curStep == 221 then
		--flash(0,0,0,1.5)
		setActorAlpha(1, bob)
		changeDadCharacter("pico")
		visibleChar("tankman",true,false)
	end
	if curStep == 164 or curStep == 223 then
		tweenFadeOut(bob, 0, 2)
	end
end