package;

import flixel.FlxSprite;

class HealthIcon extends FlxSprite
{
	/**
	 * Used for FreeplayState! If you use it elsewhere, prob gonna annoying
	 */
	public var sprTracker:FlxSprite;
	public var char:String = "bf";

	public function new(char:String = 'bf', isPlayer:Bool = false)
	{
		super();

		this.char = char;
		
		loadGraphic(Paths.image('iconGrid'), true, 150, 150);

		switch(char){
		case 'daidemAnim':
		{
			var tex = Paths.getSparrowAtlas('iconos/IconAssets', 'shared');
			frames = tex;
			antialiasing = true;
			animation.addByPrefix('daidem', 'DaidemNormal0', 24, true, isPlayer);
			animation.addByPrefix('daidemLosing', 'DaidemLoosing0', 24, true, isPlayer);
			animation.play("daidem");
		}
		default:
			{
				antialiasing = true;
				animation.add('bf', [0, 1], 0, false, isPlayer);
				animation.add('bf-car', [0, 1], 0, false, isPlayer);
				animation.add('bf-christmas', [0, 1], 0, false, isPlayer);
				animation.add('bf-pixel', [21, 21], 0, false, isPlayer);
				animation.add('spooky', [2, 3], 0, false, isPlayer);
				animation.add('pico', [4, 5], 0, false, isPlayer);
				animation.add('mom', [6, 7], 0, false, isPlayer);
				animation.add('mom-car', [6, 7], 0, false, isPlayer);
				animation.add('tankman', [8, 9], 0, false, isPlayer);
				animation.add('face', [10, 11], 0, false, isPlayer);
				animation.add('dad', [12, 13], 0, false, isPlayer);
				animation.add('senpai', [22, 22], 0, false, isPlayer);
				animation.add('senpai-angry', [22, 22], 0, false, isPlayer);
				animation.add('spirit', [23, 23], 0, false, isPlayer);
				animation.add('bf-old', [14, 15], 0, false, isPlayer);
				animation.add('gf', [16], 0, false, isPlayer);
				animation.add('gf-christmas', [16], 0, false, isPlayer);
				animation.add('gf-pixel', [16], 0, false, isPlayer);
				animation.add('parents-christmas', [17, 18], 0, false, isPlayer);
				animation.add('monster', [19, 20], 0, false, isPlayer);
				animation.add('monster-christmas', [19, 20], 0, false, isPlayer);
				animation.add('beat', [24, 25], 0, false, isPlayer);
				animation.add('keen', [26, 27], 0, false, isPlayer);
				animation.add('bf-keen', [26, 27], 0, false, isPlayer);
				animation.add('OJ', [28, 29], 0, false, isPlayer);
				animation.add('whitty', [30, 31], 0, false, isPlayer);
				animation.add('hex', [32, 33], 0, false, isPlayer);
				animation.add('sarv', [34, 35], 0, false, isPlayer);
				animation.add('ruv', [36, 37], 0, false, isPlayer);
				animation.add('bf-tankman-pixel', [8, 9], 0, false, isPlayer);
				animation.add('impostor', [38, 39], 0, false, isPlayer);
				animation.add('purple', [10, 11], 0, false, isPlayer);
				animation.add('daidem', [40, 41], 0, false, isPlayer);
				animation.play(char);

				switch(char)
				{
					case 'bf-pixel' | 'senpai' | 'senpai-angry' | 'spirit' | 'gf-pixel':
						antialiasing = false;
				}
			}
		}

		scrollFactor.set();
	}

	override function update(elapsed:Float)
	{
		super.update(elapsed);

		if (sprTracker != null)
			setPosition(sprTracker.x + sprTracker.width + 10, sprTracker.y - 30);
	}
}
